package com.GWTTable.client;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.ListHandler;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
//import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.SelectionChangeEvent;
//import com.google.gwt.view.client.SingleSelectionModel;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GWTTable implements EntryPoint {

	/**
	 * This is the entry point method.
	 */

	public void onModuleLoad() {

	
		
		
		CellTable<Address> cellTableOfAddress = new CellTable<Address>();
		cellTableOfAddress.setAutoHeaderRefreshDisabled(true);
		cellTableOfAddress.setAutoFooterRefreshDisabled(true);
		
		// The policy that determines how keyboard selection will work. Keyboard
		// selection is enabled.
		cellTableOfAddress.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

		// Add a text columns to show the details.
//		TextColumn<Address> columnFirstLine = new TextColumn<Address>() {
//			@Override
//			public String getValue(Address object) {
//				return object.getFirstLineOfAddress();
//			}
//		};
		
		
		 Column<Address, String> columnFirstLine = new Column<Address, String>(
			        new EditTextCell()) {
			      @Override
			      public String getValue(Address object) {
			        return object.getFirstLineOfAddress();
			      }
			    };
		
		
		columnFirstLine.setSortable(true);
		cellTableOfAddress.addColumn(columnFirstLine, "First line");

		TextColumn<Address> columnSecondLine = new TextColumn<Address>() {
			@Override
			public String getValue(Address object) {
				return object.getSecondLineOfAddress();
			}
		};
		
		
		columnSecondLine.setSortable(true);
		
		cellTableOfAddress.addColumn(columnSecondLine, "Second line");

		TextColumn<Address> townColumn = new TextColumn<Address>() {
			@Override
			public String getValue(Address object) {
				return object.getTown();
			}
		};
		townColumn.setSortable(true);
		cellTableOfAddress.addColumn(townColumn, "Town");

		TextColumn<Address> countryColumn = new TextColumn<Address>() {
			@Override
			public String getValue(Address object) {
				return object.getCountry();
			}
		};
		
		countryColumn.setSortable(true);
		cellTableOfAddress.addColumn(countryColumn, "Country");
		
		
		final MultiSelectionModel<Address> selectionModel = new MultiSelectionModel<Address>();
		cellTableOfAddress.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {

			public void onSelectionChange(SelectionChangeEvent event) {

				Set<Address> selectedAddress = selectionModel.getSelectedSet();
				if (selectedAddress != null) {
					
					StringBuilder sBuilder  = new StringBuilder();
					for(Address a: selectedAddress){
						
						String line = "Selected: First line: " + a. getFirstLineOfAddress() + ", Second line: " + a.getSecondLineOfAddress();
						sBuilder.append(line + "\n");
					}
					Window.alert(sBuilder.toString());
				}
			}
		});
		
			    
		List<Address> addresses = new ArrayList<Address>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("London University", "London", "London", "UK"));
				add(new Address("Leicester University", "Leicester ", "Leicester", "UK"));
				add(new Address("Lancashir Universitye", "Lancashir", "Lancashir", "UK"));
				add(new Address("Brighton University", "Brighton,  ", "Brighton", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));
				add(new Address("Oxford University", "Wallington square", "Oxford", "UK"));
				add(new Address("Cambrige University", "Trinity Ln, Cambridge ", "Cambrige", "UK"));

			}
		};
		
		cellTableOfAddress.setRowCount(addresses.size(), true);
		cellTableOfAddress.setRowData(0, addresses);
		
		cellTableOfAddress.setVisibleRange(0, addresses.size());
		
		VerticalPanel vp = new VerticalPanel();
		vp.setBorderWidth(1);
////		vp.add(flexTable);
		vp.add(cellTableOfAddress);
		
		
		
		  
	      //Create an empty tab panel 
	      TabPanel tabPanel = new TabPanel();

	      //create contents for tabs of tabpanel
//	      label1.setHeight("200");
	      Label label2 = new Label("This is contents of TAB 2");
//	      label2.setHeight("200");
	      Label label3 = new Label("This is contents of TAB 3");
//	      label3.setHeight("200");

	      //create titles for tabs
	      String tab1Title = "TAB 1";
	      String tab2Title = "TAB 2";
	      String tab3Title = "TAB 3";

	      //create tabs 
	      tabPanel.add(cellTableOfAddress, tab1Title);
	      tabPanel.add(label2, tab2Title);
	      tabPanel.add(label3, tab3Title);

	      //select first tab
	      tabPanel.selectTab(0);

	      //set width if tabpanel
	      tabPanel.setWidth("400");

	      // Add the widgets to the root panel.
//	      RootPanel.get().add(tabPanel);
	    
	      HorizontalPanel hp = new HorizontalPanel();
	      
	      
	      TextBoxAdvanced filterBox=new TextBoxAdvanced();
	      filterBox.setTitle(" Search");
	      hp.add(filterBox);
	      
	      
	      vp.add(hp);
	        
	      vp.add(tabPanel);

	      SimplePager simplePager = null;//= new SimplePager();
	  //	
//	  		simplePager.setDisplay(cellTableOfAddress);
//	  		simplePager.setPageSize(3);		
	  		
	  		
	  		
	  		  SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
	  		  simplePager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
//	      SimplePager simplePager =  new SimplePager(); 
	      simplePager.setPageSize(3);
	  		  simplePager.setDisplay(cellTableOfAddress);
	  		  simplePager.setRangeLimited(true);
	  		  vp.add(simplePager);
	  		  
			RootPanel.get("container").add(vp);
			
		
		
	    // Create a data provider.
//	    ListDataProvider<Address> dataProvider = new ListDataProvider<Address>();
			
		     final FilteredListDataProvider<Address> dataProvider = new FilteredListDataProvider<Address>(new IFilter<Address>() {
		        @Override
		        public boolean isValid(Address value, String filter) {
		            if(filter==null || value==null)
		                return true;
		            return value.getFirstLineOfAddress().toLowerCase().contains(filter.toLowerCase()) ||
		            		value.getSecondLineOfAddress().toLowerCase().contains(filter.toLowerCase());
		            		
		        }

				
		    });


	    // Connect the table to the data provider.
	    dataProvider.addDataDisplay(cellTableOfAddress);


	    // Add the data to the data provider, which automatically pushes it to the
	    // widget.
	    List<Address> list = dataProvider.getList();
	    for (Address addresse : addresses) {
	      list.add(addresse);
	    }
	    
	    filterBox.addValueChangeHandler(new IStringValueChanged() {
            @Override
            public void valueChanged(String newValue) {
            	dataProvider.setFilter(newValue);
            	dataProvider.refresh();
            }
        });

	    //dataProvider.setList(list);

	    // Add a ColumnSortEvent.ListHandler to connect sorting to the
	    // java.util.List.
	    ListHandler<com.GWTTable.client.Address> columnSortHandler = new ListHandler< com.GWTTable.client.Address>(
	        list);
	    columnSortHandler.setComparator(columnFirstLine,
	        new Comparator<com.GWTTable.client.Address>() {
	        @Override
	        public int compare(com.GWTTable.client.Address o1,com.GWTTable.client.Address o2) {
	            if (o1 == o2) {
	                  return 0;
	                }

	                // Compare the name columns.
	                if (o1 != null) {
	                  return (o2 != null) ? o1.getFirstLineOfAddress().compareTo(o2.getFirstLineOfAddress()) : 1;
	                }
	                return -1;
	        }
	        });
	    cellTableOfAddress.addColumnSortHandler(columnSortHandler);

	    // We know that the data is sorted alphabetically by default.
	    cellTableOfAddress.getColumnSortList().push(columnFirstLine);	    
    }

	

	private FlexTable createFlexTable() {
		FlexTable flexTable = new FlexTable();
		flexTable.setBorderWidth(1);
		flexTable.setText(0, 0, "This is an example of flextable");
		flexTable.setText(2, 2, "This is also an example of flextable");
		flexTable.getFlexCellFormatter().setColSpan(1, 0, 3);
		return flexTable;
	}
}
