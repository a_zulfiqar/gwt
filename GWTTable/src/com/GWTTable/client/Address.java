package com.GWTTable.client;

public class Address {
	
	private String firstLine;
	private String secondLine;
	private String town;
	private String country;

	public Address(String firstLine, String secondLine, String town, String country) {
		this.firstLine = firstLine;
		this.secondLine = secondLine;
		this.town = town;
		this.country = country;
	}
	
	public String getFirstLineOfAddress() {
		return this.firstLine;
	}
	
	public String getSecondLineOfAddress() {
		return this.secondLine;
	}
	
	public String getTown() {
		return this.town;
	}
	
	public String getCountry() {
		return this.country;
	}

}
